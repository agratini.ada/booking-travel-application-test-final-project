# Booking Travel Application Test


**Short Description**

This application is responsible for testing a few of the main functionalities and features of the web application BookingTravel:

* Login feature
* Footer elements
* Header element
* Navigation bar elements
* Reset password button
* Change language
* Search hotel and entire home
* Choose hotel by star ranking
* Select hotel


### Booking Travel Application Test Automation Framework

#### A brief presentation of my project:

This is the final project for Ada Agratini within the FastTrackIt Test Automation course.

**Test Engineer**: Ada Agratini

**Technologies used:**

- Java JDK-17
- Maven version 4.0.0
- Selenide version 6.12.4
- Allure version 2.21.0
- TestNG
- Page Object Model

### How to run the tests

git clone: https://gitlab.com/agratini.ada/booking-travel-application-test-final-project.git

### Execute all tests

* mvn clean test

### Generate Report

* mvn allure:report

### Open and present report

* mvn allure:serve

### Page objects tested:

* Header
* Footer
* Search bar
* Navigation bar
* Login modal
* Hotels Page
* Reset Your Password Page

### **Login Test**

| TEST NAME | STATUS |
|----------|--------|
|    Modal components are displayed   | PASSED |
|   User can login on page with valid credentials | PASSED |
|    User cannot login without password     | PASSED |
|    User cannot login with invalid email  | PASSED |
|   User cannot login with invalid password       | PASSED |

### **Static Application Test**

| TEST NAME                                                | STATUS |
|----------------------------------------------------------|--------|
| Verify the footer copy details are displayed             | PASSED |
| Verify the footer brand logo is displayed                | PASSED |
| Verify the facebook icon is displayed                    | PASSED |
| Verify the brand logo on the navigation bar is displayed | PASSED |
| Verify Home button is displayed                          | PASSED |
| Verify Hotel button is displayed                         | PASSED |
| Verify Entire Home button is displayed                   | PASSED |
| Verify List Your Property button is displayed            | PASSED |

### **Reset Your Password Test**

| TEST NAME                                    | STATUS |
|----------------------------------------------|--------|
| The Remember Your Password button is enabled | PASSED |

### **Search Test**

| TEST NAME                                          | STATUS      |
|----------------------------------------------------|-------------|
| User can search for Entire Home for Search Bar     | PASSED      |
| User can search for Hotel from Search Bar          | PASSED      |
| The search button on the navigation bar is enabled | PASSED      |
| User can choose hotel                              | PASSED      |

### **Change Language Test**

| TEST NAME                            | STATUS |
|--------------------------------------|--------|
| User can change application Language | PASSED |
|                                      |        |

### **Sorting Hotels**

| TEST NAME                     | PASSED |
|-------------------------------|--------|
| User can select 2 star hotels | PASSED |
| User can select 3 star hotels | PASSED |
| User can select 4 star hotels | PASSED |
| User can select 5 star hotels | PASSED |




