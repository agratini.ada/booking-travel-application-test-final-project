package org.fasttrackit;

import org.fasttrackit.body.Header;
import org.fasttrackit.body.Modal;
import org.fasttrackit.pages.HotelsPage;
import org.fasttrackit.pages.MainPage;

public class BookingTravelApp {

    public static final String BOOKING_TRAVEL_TITLE = "BookingTravel";
    private static final String APP_TITLE = "BookingTravel Testing simulator";

    public static void main(String[] args) {
        System.out.println(APP_TITLE);
        MainPage homePage = new MainPage();

        verifyStaticPage(homePage);
        verifyLoginModal(homePage);
        verifyHotelsPage(homePage);
        loginWithUser(homePage);
        validateLoggedInUser(homePage);

    }
    private static void verifyStaticPage(MainPage homePage) {
        homePage.getPageTitle();
        homePage.validateFooterContainsAllElements();
        homePage.validateHeaderContainsAllElements();
        homePage.validateNavigationBarContainsAllElements();
        homePage.validateSearchBarContainsAllElements();
    }

    private static void validateLoggedInUser(MainPage homePage) {
        Header loggedInHeader = new Header("admin@bookingtravel.info");
        homePage.setHeader(loggedInHeader);
        homePage.validateHeaderContainsAllElements();
    }

    private static void loginWithUser(MainPage homePage) {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.clickOnEmailField();
        modal.typedInEmailField("admin@bookingtravel.info");
        modal.clickOnPasswordField();
        modal.typedInPasswordField("987654321");
        modal.clickOnSubmitLoginButton();
    }

    private static void verifyHotelsPage(MainPage homePage) {
        homePage.clickOnTheHotelButton();
        HotelsPage hotelsPage = new HotelsPage();
        hotelsPage.validateHotelsPageIsDisplayed();
        hotelsPage.clickOnTheHomeButton();
    }

    private static void verifyLoginModal(MainPage homePage) {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.validateModalComponents();
        modal.clickOnCloseButton();
        homePage.validateThatModalIsNotOnPage();
    }
}