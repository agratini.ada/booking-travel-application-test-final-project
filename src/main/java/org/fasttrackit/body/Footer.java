package org.fasttrackit.body;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Footer {
    private final SelenideElement copyRightDetails = $(".copy-right");
    private final SelenideElement brandLogo = $("span.brand");

    public Footer() {
    }

    /**
     * Getters
     */

    public String getCopyRightDetails() {
        System.out.println("Copy right details are displayed");
        return this.copyRightDetails.getText();
    }

    public String getBrandLogo() {
        System.out.println("Brand logo is displayed");
        this.brandLogo.scrollTo();
        return this.brandLogo.getText();
    }
}
