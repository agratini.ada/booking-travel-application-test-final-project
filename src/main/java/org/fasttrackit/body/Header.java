package org.fasttrackit.body;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class Header {

    private final SelenideElement facebookIcon = $(".socials");
    private final SelenideElement phoneIcon = $(" div.d-flex.align-items-center.text-white.px-3 > i");
    private final SelenideElement englishLanguage = $(".currency-select:nth-child(3)");
    private final SelenideElement romanianLanguage = $("ul.show.dropdown-menu");
    private final SelenideElement signInLoginButton = $("[data-target='#login'] span");
    private final SelenideElement loggedOutButton = $(".dropdown-menu-user .fa-sign-out");

    private final SelenideElement profileButton = $("span.dropdown-nav-link:nth-child(2)");

    private final String greetingsMessage;

    public Header() {
        this.greetingsMessage = "Sign in or Register";
    }

    public Header(String user) {
        this.greetingsMessage = "Hi, Alexandru Gabriel";
    }

    /**
     * Getters
     */
    public SelenideElement getFacebookIcon() {
        return this.facebookIcon;
    }

    public SelenideElement getPhoneIcon() {
        return this.phoneIcon;
    }

    public String getLanguageMenu() {
        return this.englishLanguage.getText();
    }

    public SelenideElement getSignInLoginButton() {
        return signInLoginButton;
    }

    public String getGreetingsMessage() {
        return this.greetingsMessage;
    }

    /**
     * Clicks
     */
    public void clickOnTheLoginButton() {
        System.out.println("Clicked on the Login button.");
        this.signInLoginButton.click();
        sleep(300);
    }

    public void clickOnTheProfileButton() {
        System.out.println("Clicked on the Profile button");
        this.profileButton.click();
    }

    public void clickOnLanguageMenu() {
        System.out.println("Clicked on Language Menu");
        this.englishLanguage.click();
    }

    public void clickChangeLanguage() {
        System.out.println("Changed language");
        this.romanianLanguage.click();
    }

    public void logUserOut() {
        System.out.println("User is logged out");
        this.loggedOutButton.click();
    }
}
