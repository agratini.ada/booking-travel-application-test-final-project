package org.fasttrackit.body;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.WebElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class Modal {

    private final SelenideElement desktopMode = $(".bravo-form-login");
    private final SelenideElement modalTitle = $(".modal-header .modal-title");
    private final SelenideElement closeButton = $("#login [alt=close]");
    private final SelenideElement emailField = desktopMode.$("[name=email]");
    private final SelenideElement passwordField = desktopMode.$("[name=password]");
    private final SelenideElement errorMessage = $(".alert-danger");
    private final SelenideElement submitLoginButton = desktopMode.$(".form-submit");
    private final SelenideElement rememberYourPasswordButton = $("form > div:nth-child(5) > div > a");
    private final SelenideElement registrationButton = $("div.text-center:nth-child(9) > a:nth-child(1)");


    public Modal() {
    }

    public String getModalTitle() {
        return this.modalTitle.text();
    }

    public String getInvalidLoginMessage() {
        return this.errorMessage.getText();
    }


    /**
     * CLICKS
     */

    public void clickOnCloseButton() {
        this.closeButton.click();
        System.out.println("Clicked on the x button");
        sleep(300);
    }

    public void clickOnEmailField() {
        System.out.println("Clicked on the " + this.emailField);
    }

    public void clickOnPasswordField() {
        System.out.println("Clicked on the " + this.passwordField);
    }

    public void clickOnSubmitLoginButton() {
        System.out.println("Clicked on the Submit Login Button");
        this.submitLoginButton.click();
        sleep(300);
    }

    public void clickOnForgotPassword() {
        System.out.println("Click on Forgot Password");
        this.rememberYourPasswordButton.click();
    }

    /**
     * TYPES
     */

    public void typedInEmailField(String userEmail) {
        System.out.println("Typed in email field: " + userEmail);
        this.emailField.click();
        this.emailField.sendKeys(userEmail);
    }

    public void typedInPasswordField(String userPassword) {
        System.out.println("Typed in password field: " + userPassword);
        this.passwordField.click();
        this.passwordField.sendKeys(userPassword);
    }

    /**
     * VALIDATIONS
     */

    public void validateModalComponents() {
    }

    public boolean validateCloseButtonIsDisplayed() {
        boolean exists = this.closeButton.exists();
        boolean displayed = this.closeButton.isDisplayed();
        System.out.println("Close button exists " + exists);
        System.out.println("Close button displayed " + displayed);
        return exists && displayed;
    }

    public boolean validateEmailField() {
        System.out.println(" The e-mail field displayed");
        return this.emailField.exists() && this.emailField.isDisplayed();
    }


    public boolean validatePasswordField() {
        System.out.println("The password field is displayed");
        return this.passwordField.exists() && this.passwordField.isDisplayed();
    }

    public boolean validateSubmitLoginButton() {
        System.out.println("The Submit Login Button is displayed");
        return this.submitLoginButton.exists() && this.submitLoginButton.isDisplayed();
    }

    public boolean validateRegistrationButton() {
        System.out.println("Registration button is displayed");
        return this.registrationButton.exists() && this.registrationButton.isDisplayed();
    }


    public void clearEmailField() {
        System.out.println("The email field is clear");
        this.emailField.clear();

    }

    public void clearPasswordField() {
        System.out.println("The password field is clear");
        this.passwordField.clear();
    }

}
