package org.fasttrackit.body;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class NavigationBar {

    private final SelenideElement brandLogo = $(".bravo_header .header-left .bravo-logo .u-header__navbar-brand-text");
    private final SelenideElement homeButton = $(".bravo-menu .main-menu [href='/']");
    private final SelenideElement hotelButton = $(".bravo-menu .main-menu .depth-0>[href='/hotel']");
    private final SelenideElement entireHomeButton = $(".bravo-menu .main-menu .depth-0>[href='/space']");
    private final SelenideElement listYourPropertyButton = $("#header div.bravo_header .header-left ul > li:nth-child(4) > a");

    /**
     * Getters
     */
    public String getBrandLogo() {
        System.out.println("The Brand logo on the navigation bar is displayed");
        return this.brandLogo.getText();
    }

    public String getHomeButton() {
        System.out.println("The Home button on the navigation bar is displayed");
        return this.homeButton.getText();
    }

    public String getHotelButton() {
        System.out.println("The Hotel button on the navigation bar is displayed");
        return this.hotelButton.getText();
    }

    public String getEntireHomeButton() {
        System.out.println("The Entire Home button on the navigation bar is displayed");
        return this.entireHomeButton.getText();
    }

    public String getListYourPropertyButton() {
        System.out.println("The List Your Property button on the navigation bar is displayed");
        return this.listYourPropertyButton.getText();
    }

    /**
     * Actions
     */
    public void clickOnTheHotelButton() {
        System.out.println("Clicked on the " + getHotelButton() + " button. ");
        this.hotelButton.click();
    }

    public void clickOnTheHomeButton() {
        System.out.println("Clicked on the " + getHomeButton() + " button. ");
        this.homeButton.click();
    }

}
