package org.fasttrackit.body;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class SearchBar {
    private final SelenideElement hotelTab = $("#bravo_hotel-tab");
    private final SelenideElement entireHomeTab = $("#bravo_space-tab");
    private final SelenideElement locationSearch = $(".smart-search-location");
    private final SelenideElement chooseDate = $(".date-wrapper");
    private final SelenideElement selectGuest = $("form-select-guests");
    private final SelenideElement increaseAdultNumber = $(".btn-add[data-input=adults]");
    private final SelenideElement searchButton = $(".g-button-submit");


    public SearchBar() {
    }


    public String getHotelTab() {
        System.out.println("Hotel Tab is displayed");
        return this.hotelTab.getText();
    }

    public String getEntireHomeTab() {
        System.out.println("Entire Home Tab is displayed");
        return this.entireHomeTab.getText();
    }

    public SelenideElement getLocationSearch() {
        System.out.println("Location field on the search bar is displayed");
        return this.locationSearch;
    }

    public SelenideElement getChooseDate() {
        System.out.println("The calendar on the search bar is displayed");
        return this.chooseDate;
    }

    public SelenideElement getSelectGuest() {
        System.out.println("Guest type selection is displayed");
        return this.selectGuest;
    }

    public SelenideElement getSearchButton() {
        System.out.println("Search button is displayed");
        return this.searchButton;
    }


    /**
     * Clicks
     */

    public void clickOnEntireHomeTab() {
        this.entireHomeTab.click();
    }

    public void clickOnHotelTab() {
        this.hotelTab.click();
        sleep(300);
    }


    public void clickOnSearchButton() {
        System.out.println("Clicked on the search button");
        this.searchButton.click();
    }




}


