package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class HotelsPage extends MainPage {
    private final SelenideElement sortMenu = $(".dropdown>.dropdown-toggle");
    private final SelenideElement sortLowToHigh = $("[href='https://test.bookingtravel.info/hotel?orderby=price_low_high']");
    private final SelenideElement threeStarHotels = $("#rate div:nth-child(1) div:nth-child(3) div:nth-child(1) label:nth-child(2)");
    private SelenideElement hotelChosen = $(".product-item__outer [src='https://test.bookingtravel.info/uploads/demo/space/space-9.jpg']");
    private SelenideElement fourStarHotels = $("#rate div:nth-child(1) div:nth-child(2) div:nth-child(1) label:nth-child(2)");
    ;
    private SelenideElement fiveStarHotels = $("#rate div:nth-child(1) div:nth-child(1) div:nth-child(1)");
    ;
    private SelenideElement twoStarHotels = $("#rate div:nth-child(1) div:nth-child(4) div:nth-child(1) label:nth-child(2)");
    ;

    public HotelsPage() {
        System.out.println("Hotels Results Page is created");
    }

    public void validateHotelsPageIsDisplayed() {
        System.out.println("Verify that the Hotels MainPage is displayed.");
    }

    public void clickOnHotelChosen() {
        System.out.println("Hotel is chosen");
        this.hotelChosen.click();
    }

    public void clickOnSortMenu() {
        this.sortMenu.click();
    }

    public void clickOnSortLowToHigh() {
        this.sortLowToHigh.click();
    }

    public void clickOnTwoStarHotels() {
        System.out.println("Two star hotels are listed");
        this.twoStarHotels.click();
    }

    public void clickOnThreeStarHotels() {
        System.out.println("Three star hotels are displayed");
        this.threeStarHotels.click();
    }

    public void clickOnFourStarHotels() {
        System.out.println("Four star hotels are listed");
        this.fourStarHotels.click();
    }

    public void clickOnFiveStarHotels() {
        System.out.println("Five star hotels are listed");
        this.fiveStarHotels.click();
    }
}
