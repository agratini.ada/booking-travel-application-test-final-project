package org.fasttrackit.pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.fasttrackit.body.Footer;
import org.fasttrackit.body.Header;
import org.fasttrackit.body.NavigationBar;
import org.fasttrackit.body.SearchBar;
import org.fasttrackit.head.Head;

import static com.codeborne.selenide.Selenide.$;

public class MainPage extends Page {

    private final String title = Selenide.title();
    private final Head head;
    private final SelenideElement modal = $(".modal-open");
    private final SelenideElement homePageMessage = $(".py-8 h1");
    private final NavigationBar navigationBar;
    private final SearchBar searchBar;
    private final Footer footer;
    private Header header;


    public MainPage() {
        System.out.println("Constructing Head");
        this.head = new Head();
        System.out.println("Constructing Header");
        this.header = new Header();
        System.out.println("Constructing Search Bar");
        this.searchBar = new SearchBar();
        System.out.println("Constructing Navigation Bar ");
        this.navigationBar = new NavigationBar();
        System.out.println("Constructing Footer");
        this.footer = new Footer();
    }

    /**
     * Getters
     */
    public String getPageTitle() {
        return title;
    }

    public String getHomePageMessage() {
        return this.homePageMessage.getText();
    }

    public Header getHeader() {
        return this.header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public SearchBar getSearchBar() {
        return this.searchBar;
    }

    public Footer getFooter() {
        return this.footer;
    }

    public NavigationBar getNavigationBar() {
        return navigationBar;
    }

    /**
     * VALIDATIONS
     */

    public void validateFooterContainsAllElements() {
    }

    public void validateHeaderContainsAllElements() {
        System.out.println("2. Verify that phone logo icon is: " + header.getPhoneIcon());
        System.out.println("3. Verify that language menu is: " + header.getLanguageMenu());
        System.out.println("4. Verify that Login button is: " + header.getSignInLoginButton());
        System.out.println("5. Verify that greetings message is: " + header.getGreetingsMessage());
    }

    public boolean validateThatModalIsDisplayed() {
        System.out.println("Verify that the modal is displayed on page");
        return this.modal.exists() && this.modal.isDisplayed();
    }

    public boolean validateThatModalIsNotOnPage() {
        System.out.println("Modal is not on page.");
        return !this.modal.exists();
    }

    public void validateNavigationBarContainsAllElements() {
    }

    public void validateSearchBarContainsAllElements() {
        System.out.println("1. Verify that Hotel search option is displayed: " + searchBar.getHotelTab());
        System.out.println("2. Verify that Entire Home search option is displayed: " + searchBar.getEntireHomeTab());
        System.out.println("4. Verify that location search field is " + searchBar.getLocationSearch());
        System.out.println("6. Verify that the date picker field is " + searchBar.getChooseDate());
        System.out.println("8. Verify that the guest input field is " + searchBar.getSelectGuest());
        System.out.println("9. Verify that the Search button is " + searchBar.getSearchButton());
    }

    /**
     * Clicks
     */

    public void clickOnTheLoginButton() {
        this.header.clickOnTheLoginButton();
    }

    public void clickOnTheHotelButton() {
        this.navigationBar.clickOnTheHotelButton();
    }

    public void clickOnTheHomeButton() {
        this.navigationBar.clickOnTheHomeButton();
    }

}

