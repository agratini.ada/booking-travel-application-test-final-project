package org.fasttrackit.pages;

import static com.codeborne.selenide.Selenide.open;

public class Page {

    public Page() {
        System.out.println("Opening MainPage");
        open("https://test.bookingtravel.info/");
    }
}
