package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;
import org.fasttrackit.body.NavigationBar;

import static com.codeborne.selenide.Selenide.$;

public class ResetYourPasswordPage extends NavigationBar {
    private final NavigationBar navigationBar = new NavigationBar();

    private final SelenideElement cardBodyTitle = $("body > div > div.container > div > div > div > div.card-header");
    
    
    public String getCardBodyTitle() {
        return this.cardBodyTitle.getText();
    }

    public NavigationBar getNavigationBar() {
        return navigationBar;
    }
}
