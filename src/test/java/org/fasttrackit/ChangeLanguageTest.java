package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.configuration.TestConfiguration;
import org.fasttrackit.pages.HotelsPage;
import org.fasttrackit.pages.MainPage;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class ChangeLanguageTest extends TestConfiguration {

    MainPage homePage = new MainPage();


    @BeforeClass
    public void tearUp() {
        homePage = new MainPage();
        System.out.println("Home page will open before each test class");
    }

    @AfterClass
    public void tearDown() {
        Selenide.closeWindow();
        System.out.println("Browser window will close after each test class");
    }

    @Test(testName = "User can change application language", description = "This test verifies that the user can change the language from the language menu from English to Romanian and back")
    @Severity(SeverityLevel.NORMAL)
    public void user_can_change_language() {
        homePage.getHeader().clickOnLanguageMenu();
        homePage.getHeader().clickChangeLanguage();
        Selenide.back();
        String entireHomeButton = homePage.getNavigationBar().getEntireHomeButton();
        assertEquals(entireHomeButton, "ENTIRE HOME", "Expected Entire Home Button: ENTIRE HOME BUTTON");
    }
}
