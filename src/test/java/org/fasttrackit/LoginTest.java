package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.body.Header;
import org.fasttrackit.body.Modal;
import org.fasttrackit.configuration.TestConfiguration;
import org.fasttrackit.pages.MainPage;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@Feature("User Login")
public class LoginTest extends TestConfiguration {

    MainPage homePage = new MainPage();

    @BeforeClass
    public void tearUp() {
        homePage = new MainPage();
        System.out.println("Home page will open before each test class");
    }

    @AfterClass
    public void tearDown() {
        Selenide.closeWindow();
        System.out.println("Browser window will close after each test class");
    }

    @Test(testName = "Modal components are displayed", description = "This test verifies that the login modal components are displayed and the login modal disappears after clicking on the close button")
   @Severity(SeverityLevel.NORMAL)
    public void modal_components_are_displayed() {
        homePage.clickOnTheLoginButton();
        assertTrue(homePage.validateThatModalIsDisplayed(), "Expected modal is displayed");
        Modal modal = new Modal();
        sleep(500);
        assertTrue(modal.validateCloseButtonIsDisplayed(), "Expected modal close button is displayed");
        assertTrue(modal.validateEmailField(), "Expected e-mail field is displayed");
        assertTrue(modal.validatePasswordField(), "Expected password field is displayed");
        assertTrue(modal.validateSubmitLoginButton(), "Expected login button is displayed");
        assertTrue(modal.validateRegistrationButton(), "Expected registration button is displayed");
        modal.clickOnCloseButton();
        assertTrue(homePage.validateThatModalIsNotOnPage(), "Expected modal is not displayed.");
    }


    @Test(testName = "User can login on page with valid credentials", description = "This test verifies that the user can login on page with a valid e-mail address and valid password")
    @Severity(SeverityLevel.BLOCKER)
    public void user_can_login_on_page() {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.clearEmailField();
        modal.typedInEmailField("admin@bookingtravel.info");
        modal.typedInPasswordField("987654321");
        modal.clickOnSubmitLoginButton();
        Header header = new Header("admin@bookingtravel.info");
        assertEquals(header.getGreetingsMessage(), "Hi, Alexandru Gabriel", "Expected greetings message to be:Hi, Alexandru Gabriel");
        sleep(3000);
        header.clickOnTheProfileButton();
        header.logUserOut();
        sleep(1000);
    }

    @Test(testName = "User cannot login without password", description = "This test verifies that the user cannot login on page without typing a password")
    @Severity(SeverityLevel.BLOCKER)
    public void user_cannot_login_without_password_test() {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.clearEmailField();
        modal.typedInEmailField("admin@bookingtravel.info");
        modal.clickOnSubmitLoginButton();
        sleep(3000);
        assertEquals(modal.getInvalidLoginMessage(), "The given data was invalid.", "Expected invalid login message to be:The given data was invalid.");
        modal.clickOnCloseButton();
    }

    @Test(testName = "User cannot login with invalid email", description = "This test verifies that the user cannot login on page with an invalid e-mail")
    @Severity(SeverityLevel.BLOCKER)
    public void user_cannot_login_with_invalid_email() {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.clearEmailField();
        String userEmail = "alina@bookingtravel.info";
        modal.typedInEmailField(userEmail);
        modal.typedInPasswordField("987654321");
        modal.clickOnSubmitLoginButton();
        assertEquals(modal.getInvalidLoginMessage(), "The given data was invalid.", "Expected invalid login message to be:The given data was invalid.");
        modal.clickOnCloseButton();
        sleep(300);
    }

    @Test(testName = "User cannot login with invalid password", description = "This test verifies that the user cannot login with an invalid password")
    @Severity(SeverityLevel.BLOCKER)
    public void user_cannot_login_with_invalid_password() {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.clearEmailField();
        modal.clearPasswordField();
        String userPassword = "12345678";
        modal.typedInEmailField("alina@bookingtravel.info");
        modal.typedInPasswordField(userPassword);
        modal.clickOnSubmitLoginButton();
        sleep(3000);
        assertEquals(modal.getInvalidLoginMessage(), "The given data was invalid.", "Expected invalid login message to be:The given data was invalid.");
        modal.clickOnCloseButton();
        sleep(300);
    }


}