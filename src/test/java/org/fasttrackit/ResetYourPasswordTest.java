package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import org.fasttrackit.body.Modal;
import org.fasttrackit.configuration.TestConfiguration;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.pages.ResetYourPasswordPage;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class ResetYourPasswordTest extends TestConfiguration {

    MainPage homePage = new MainPage();

    @BeforeClass
    public void tearUp() {
        homePage = new MainPage();
        System.out.println("Home page will open before each test class");
    }

    @AfterClass
    public void tearDown() {
        Selenide.closeWindow();
        System.out.println("Browser window will close after each test class");
    }

    @Test(testName = "The remember your password button is enabled", description = "This test verifies that if the user clicks on Forgot Your Password, a new page is displayed where you can reset your password by filling in your e-mail address in a field")
    @Severity(SeverityLevel.BLOCKER)
    public void remember_your_password_button_is_enabled() {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.clickOnForgotPassword();
        ResetYourPasswordPage resetYourPasswordPage = new ResetYourPasswordPage();
        String cardBodyTitle = resetYourPasswordPage.getCardBodyTitle();
        assertEquals(cardBodyTitle, "Reset Password", "Expected card body title to be: Reset Password");
        resetYourPasswordPage.getNavigationBar().clickOnTheHomeButton();
        String homePageMessage = homePage.getHomePageMessage();
        assertEquals(homePageMessage, "Let's travel together!", "Expected home page message to be: Let's travel together!");
    }
}
