package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.configuration.TestConfiguration;
import org.fasttrackit.pages.HotelsPage;
import org.fasttrackit.pages.MainPage;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@Feature("Search")
public class SearchTest extends TestConfiguration {

    MainPage homePage = new MainPage();
    HotelsPage hotelsPage = new HotelsPage();

    @BeforeClass
    public void tearUp() {
        homePage = new MainPage();
        System.out.println("Home page will open before each test class");
    }

    @AfterClass
    public void tearDown() {
        Selenide.closeWindow();
        System.out.println("Browser window will close after each test class");
    }


    @Test(testName = "User can search for Entire Home from search bar", description = "This test verifies that the user can search for an entire home by selecting the Entire Home Tab and clicking on the search button from the search bar")
    @Severity(SeverityLevel.CRITICAL)
    public void user_can_search_for_entire_home_from_searchbar() {
        homePage.getSearchBar().getEntireHomeTab();
        homePage.getSearchBar().clickOnSearchButton();
        hotelsPage.getNavigationBar().clickOnTheHomeButton();
        String homePageMessage = homePage.getHomePageMessage();
        assertEquals(homePageMessage, "Let's travel together!", "Expected home page message to be: Let's travel together!");
    }

    @Test(testName = "User can search for Hotel from search bar", description = "This test verifies that the user can search for a hotel by selecting the Hotel Tab and clicking on the search button from the search bar")
    @Severity(SeverityLevel.CRITICAL)
    public void user_can_search_for_hotel_from_searchbar() {
        homePage.getSearchBar().getHotelTab();
        homePage.getSearchBar().getSearchButton();
        hotelsPage.getNavigationBar().clickOnTheHomeButton();
        String homePageMessage = homePage.getHomePageMessage();
        assertEquals(homePageMessage, "Let's travel together!", "Expected home page message to be: Let's travel together!");

    }


    @Test(testName = "The search button on the navigation bar is enabled", description = "This test verifies that the search button function on the navigation bar is enabled")
    @Severity(SeverityLevel.CRITICAL)
    public void the_search_button_on_the_navigation_bar_is_enabled() {
        homePage.getSearchBar().clickOnSearchButton();
        homePage.getNavigationBar().clickOnTheHomeButton();
        String homePageMessage = homePage.getHomePageMessage();
        assertEquals(homePageMessage, "Let's travel together!", "Expected home page message to be: Let's travel together!");
    }

    @Test(testName = "User can choose a hotel", description = "The user can select a hotel from the listed hotel results after clicking on the search button on the Home Page")
    @Severity(SeverityLevel.CRITICAL)
    public void user_can_select_a_hotel() {
        homePage.clickOnTheHotelButton();
        hotelsPage.clickOnHotelChosen();
        hotelsPage.getNavigationBar().clickOnTheHomeButton();
        String homePageMessage = homePage.getHomePageMessage();
        assertEquals(homePageMessage, "Let's travel together!", "Expected home page message to be: Let's travel together!");
    }


}




