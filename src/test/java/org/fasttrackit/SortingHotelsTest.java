package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.configuration.TestConfiguration;
import org.fasttrackit.pages.HotelsPage;
import org.fasttrackit.pages.MainPage;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class SortingHotelsTest extends TestConfiguration {

    MainPage homePage = new MainPage();
    HotelsPage hotelsPage = new HotelsPage();

    @BeforeClass
    public void tearUp() {
        homePage = new MainPage();
        System.out.println("Home page will open before each test class");
    }

    @AfterClass
    public void tearDown() {
        Selenide.closeWindow();
        System.out.println("Browser window will close after each test class");
    }

    @Test(testName = "User can select 2 star hotels", description = "This test verifies that the user can choose to filter the hotel results by selecting only 2 star hotels")
    @Severity(SeverityLevel.NORMAL)
    public void user_can_select_2_star_hotels() {
        homePage.clickOnTheHotelButton();
        hotelsPage.clickOnTwoStarHotels();
        hotelsPage.getNavigationBar().clickOnTheHomeButton();
        String homePageMessage = homePage.getHomePageMessage();
        assertEquals(homePageMessage, "Let's travel together!", "Expected home page message to be: Let's travel together!");
    }

    @Test(testName = "User can select 3 star hotels", description = "This test verifies that the user can choose to filter the hotel results by selecting only 3 star hotels")
    @Severity(SeverityLevel.NORMAL)
    public void user_can_select_3_star_hotels() {
        homePage.clickOnTheHotelButton();
        hotelsPage.clickOnThreeStarHotels();
        hotelsPage.getNavigationBar().clickOnTheHomeButton();
        String homePageMessage = homePage.getHomePageMessage();
        assertEquals(homePageMessage, "Let's travel together!", "Expected home page message to be: Let's travel together!");
    }

    @Test(testName = "User can select 4 star hotels", description = "This test verifies that the user can choose to filter the hotel results by selecting only 4 star hotels")
    @Severity(SeverityLevel.NORMAL)
    public void user_can_select_4_star_hotels() {
        homePage.clickOnTheHotelButton();
        hotelsPage.clickOnFourStarHotels();
        hotelsPage.getNavigationBar().clickOnTheHomeButton();
        String homePageMessage = homePage.getHomePageMessage();
        assertEquals(homePageMessage, "Let's travel together!", "Expected home page message to be: Let's travel together!");
    }

    @Test(testName = "User can select 5 star hotels", description = "This test verifies that the user can choose to filter the hotel results by selecting only 5 star hotels")
    @Severity(SeverityLevel.NORMAL)
    public void user_can_select_5_star_hotels() {
        homePage.clickOnTheHotelButton();
        hotelsPage.clickOnFiveStarHotels();
        hotelsPage.getNavigationBar().clickOnTheHomeButton();
        String homePageMessage = homePage.getHomePageMessage();
        assertEquals(homePageMessage, "Let's travel together!", "Expected home page message to be: Let's travel together!");
    }
}
