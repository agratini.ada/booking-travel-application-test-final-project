package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.configuration.TestConfiguration;
import org.fasttrackit.pages.MainPage;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static org.testng.Assert.*;
import static org.testng.Assert.assertTrue;

public class StaticApplicationTest extends TestConfiguration {

    MainPage homePage;

    @BeforeClass
    public void tearUp() {
        homePage = new MainPage();
        System.out.println("Home page will open before each test class");
    }


    @AfterClass
    public void tearDown() {
        Selenide.closeWindow();
        System.out.println("Browser window will close after each test class");
    }

    @Test(testName = "Verify the footer copy right details are displayed", description = "This test verifies that the footer copy right details are displayed and are correct")
    @Severity(SeverityLevel.NORMAL)
    public void verify_footer_copy_right_details() {
        String footerCopyRightDetails = homePage.getFooter().getCopyRightDetails();
        assertEquals(footerCopyRightDetails, "© 2022 BookingTravel. All rights reserved", "Expected copy right details: © 2022 BookingTravel. All rights reserved");
    }

    @Test(testName = "Verify the footer brand logo is displayed", description = "This test verifies that the footer brand logo is displayed and is BookingTravel")
    @Severity(SeverityLevel.NORMAL)
    public void verify_footer_brand_logo() {
        String footerBrandLogo = homePage.getFooter().getBrandLogo();
        assertEquals(footerBrandLogo, "BookingTravel", "Expected brand logo: BookingTravel");
    }

    @Test(testName = "Verify the facebook icon is displayed", description = "This test verifies that the facebook icon is displayed")
    @Severity(SeverityLevel.NORMAL)
    public void verify_facebook_icon_is_displayed() {
        SelenideElement facebookLogoIcon = homePage.getHeader().getFacebookIcon();
        assertTrue(facebookLogoIcon.exists(), "Expected Facebook Logo Icon to exist on Main Page.");
        assertTrue(facebookLogoIcon.isDisplayed(), "Expected Facebook Logo Icon to be displayed");
    }


    @Test(testName = "Verify the brand logo on the navigation bar is displayed", description = "This test verifies that the brand logo on the navigation bar is displayed and is BookingTravel")
    @Severity(SeverityLevel.NORMAL)
    public void verify_brand_logo_button() {
        String brandLogoButton = homePage.getNavigationBar().getBrandLogo();
        assertEquals(brandLogoButton, "BookingTravel", "Expected Brand Logo is : BookingTravel");
    }

    @Test(testName = "Verify Home button is displayed", description = "This test verifies that the Home button on the navigation bar is displayed and it is named HOME")
    @Severity(SeverityLevel.NORMAL)
    public void verify_home_button() {
        String homeButton = homePage.getNavigationBar().getHomeButton();
        assertEquals(homeButton, "HOME", "Expected Home Button is: HOME");
    }

    @Test(testName = "Verify Hotel button is displayed", description = "This test verifies that Hotel button onn the navigation bar is displayed and it is named HOTEL")
    @Severity(SeverityLevel.NORMAL)
    public void verify_hotel_button() {
        String hotelButton = homePage.getNavigationBar().getHotelButton();
        assertEquals(hotelButton, "HOTEL", "Expected Hotel Button is: HOTEL");
    }

    @Test(testName = "Verify Entire Home button is displayed", description = "This test verifies that the Entire Home button is displayed and it is named ENTIRE HOME ")
    @Severity(SeverityLevel.NORMAL)
    public void verify_entire_home_button() {
        String entireHomeButton = homePage.getNavigationBar().getEntireHomeButton();
        assertEquals(entireHomeButton, "ENTIRE HOME", "Expected Entire Home Button is: ENTIRE HOME");
    }

    @Test(testName = "Verify List Your Property button is displayed", description = "This test verifies that the List Your Property button is displayed it is named LIST YOUR PROPERTY")
    @Severity(SeverityLevel.NORMAL)
    public void verify_list_your_property_button() {
        String listYourPropertyButton = homePage.getNavigationBar().getListYourPropertyButton();
        assertEquals(listYourPropertyButton, "LIST YOUR PROPERTY", "Expected List Your Property Button is: LIST YOUR PROPERTY");
    }


}




