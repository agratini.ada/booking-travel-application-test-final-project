package org.fasttrackit.configuration;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.testng.annotations.BeforeClass;

public class TestConfiguration {

    public TestConfiguration() {
        Configuration.browser ="chrome";
        Configuration.screenshots = true;
    }

    @BeforeClass
    static void setUpAllureReports() {
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide()
                .screenshots(true)
                .savePageSource(true));
    }

}


